messenger.cloudFile.onFileUpload.addListener(() => {
    return { aborted: true, };
});

messenger.cloudFile.onAccountAdded.addListener(account => {
    messenger.cloudFile.updateAccount(account.id, { configured: true, });
});
